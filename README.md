## qssi-user 11 RKQ1.211001.001 V13.0.5.0.RGEMIXM release-keys
- Manufacturer: xiaomi
- Platform: bengal
- Codename: fog
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.211001.001
- Incremental: V13.0.5.0.RGEMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/fog/fog:11/RKQ1.211001.001/V13.0.5.0.RGEMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.211001.001-V13.0.5.0.RGEMIXM-release-keys
- Repo: redmi_fog_dump_13353


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
